
#############################################################
How to compile this project: mvn clean compile assembly:single

We are creating a fat jar since this jar needs to be run as part of oozie workflow.
#############################################################
Where to run

 This jar will be run as part of below oozie workflow:
 	/user/adc/fincrime/oozie/FinCrime-Rule-Execution-workflow-v03 


 
 #############################################################
How to run –

Pre-requisites before running: 
1. Create jar and upload it to the below directory in HDFS:
		/user/adc/fincrime/java/aml-exec-engine-0.0.1-SNAPSHOT.jar
	Example: hadoop fs -put aml-exec-engine-0.0.1-SNAPSHOT.jar /user/adc/fincrime/java/aml-exec-engine-0.0.1-SNAPSHOT.jar
2. This jar will be run as part of an oozie workflow
3. Within oozie, this jar will be called from a shell action.
4. The shell script should be uploaded to the below location:
		/user/adc/fincrime/java/rule_execution.sh

This jar can be executed from oozie workflow located at below location:
	/user/adc/fincrime/oozie/FinCrime-Rule-Execution-workflow-v03 
		
		


#############################################################
Purpose of this project:
	This project is the rule engine that creates alerts and cases for each client
	

#############################################################
How to view logs:
	The logs for this can be found within oozie	

#############################################################
Project Execution Flow:

Pre-requisites before execution:
1. Rule, RuleCondition, SegmentRuleRiskMult and Client tables need to be sqooped from SQL to Hive.
	This is done via a oozie workflow - Sqoop-Sql-To-Hive
	Only approved rules are sqooped.

1. For each client and given run date, for each approved rule, generate alert details, alert transactions and then ingest into alert_master

2. For the given case date, for each customer, this method
	 * a. rolls up all alerts for a customer into a case in the case_master table (rolled up table)
	 * b. Also, creates a case_alert table that assigns a case_id to each alert for a customer(flat table)

Note: For MVP, ALert RunDate and CaseDate is subtracted by 365 days since we only have transactions for the year 2016