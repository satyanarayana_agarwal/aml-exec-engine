package com.ey.digital.adc.aml.exec.engine.repo;

import java.sql.Timestamp;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.ey.digital.adc.aml.exec.engine.model.Rule;

@Component
public class RuleEngineRepo {

	private static final Logger log = Logger.getLogger(RuleEngineRepo.class);
	
	private static final String fieldSeparatorCreateStatement = " ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'";

	public void createCaseAlertTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema) {
		StringBuffer caseAlertcreate = new StringBuffer();
		caseAlertcreate.append("create table if not exists fincrime_" + hiveSchema + ".case_alert (")
		.append("exec_id String,alert_id String, alert_dt String,focal_entity_type String, focal_entity_id String,"
				+ " cust_id String, segment String, risk String, case_id String, case_dt String )")
		.append(fieldSeparatorCreateStatement);

		hiveJdbcTemplate.execute(caseAlertcreate.toString());
		log.info("caseAlertcreate= " + caseAlertcreate);
		log.debug("Case Alert Create table statement ran successfully");

	}

	public void insertCaseAlertTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema, String caseDate,
			String caseTimestamp) {
		StringBuffer caseAlertInsert = new StringBuffer();
		caseAlertInsert.append("Insert INTO table fincrime_"+hiveSchema+".case_alert select a.exec_id,a.alert_id,a.alert_dt,a.focal_entity_type,a.focal_entity_id,")
		.append(" a.cust_id,a.segment,a.risk,c.case_id,c.case_dt from fincrime_"+hiveSchema+".alert_master a join fincrime_"+hiveSchema+".case_master c ")
		.append("on a.cust_id=c.cust_id where substring(alert_dt, 0, 7) = substring(case_dt,0,7) ");

		log.info("caseAlertInsert= " + caseAlertInsert);
		hiveJdbcTemplate.execute(caseAlertInsert.toString());
		log.debug("Case Alert Insert table statement ran successfully");

	}

	public void createCaseMasterTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema) {
		StringBuffer caseMasterCreate = new StringBuffer();
		caseMasterCreate.append("create table if not exists fincrime_" + hiveSchema + ".case_master (")
		.append("case_id String,cust_id String, month_dt String,case_dt String, alert_ct INT )")
		.append(fieldSeparatorCreateStatement);

		hiveJdbcTemplate.execute(caseMasterCreate.toString());
		log.info("caseMasterCreate= " + caseMasterCreate);
		log.debug("Case Master Create table statement ran successfully");

	}

	public void insertCaseMasterTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema, String caseDate,
			String caseTimestamp) {

		StringBuffer caseMasterInsert = new StringBuffer();
		caseMasterInsert.append(" Insert INTO table fincrime_"+hiveSchema+".case_master select CONCAT('case_',cust_id,'_','"+caseTimestamp+"') as case_id,")
		.append(" cust_id, substring(alert_dt, 0, 7) as month_dt, '"+caseDate+"' as case_dt, count(alert_id) as alert_ct")
		.append(" from fincrime_"+hiveSchema+".alert_master where substring(alert_dt, 0, 7) = substring('"+caseDate+"',0,7)")
		.append(" group by cust_id, substring(alert_dt, 0, 7)");

		hiveJdbcTemplate.execute(caseMasterInsert.toString());
		log.info("caseMaster= " + caseMasterInsert);
		log.debug("Case Master Insert table statement ran successfully");

	}

	public void insertAlertMasterTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema, String exec_id,
			String focalEntityType, String focalEntityId, String segment, String risk, int ruleId) {

		StringBuffer alertMasterInsert = new StringBuffer();
		if(focalEntityType.equalsIgnoreCase("customer")){
			alertMasterInsert.append("Insert into table fincrime_"+hiveSchema+".alert_master select exec_id ,alert_id, alert_dt,"
					+ " focalEntity, cust_id, cust_id, cust_segment, cust_risk"
					+ " from fincrime_"+hiveSchema+".alert_" + ruleId + " where exec_id='" + exec_id + "'");
		}
		else {
			alertMasterInsert.append("Insert into table fincrime_"+hiveSchema+".alert_master select a.exec_id ,a.alert_id, a.alert_dt,"
					+ "a.focalEntity, a.acct_id, av.cust_id, a.acct_segment, a.acct_risk"
					+ " from fincrime_"+hiveSchema+".alert_" + ruleId + " a join fincrime_"+hiveSchema+".account_view av "
							+ "on a.acct_id = av.acct_id where a.exec_id='" + exec_id + "'");
			
		}

		log.info("alertMasterInsert= " + alertMasterInsert);
		hiveJdbcTemplate.execute(alertMasterInsert.toString());

	}

	public void updateRuleExecutionStatus(JdbcTemplate hiveJdbcTemplate, String hiveSchema, String exec_id, int rule_id,
			String execution_status) {

		StringBuffer updateQuery = new StringBuffer();
		updateQuery.append("Insert INTO TABLE fincrime_" + hiveSchema + ".rule_execution_status VALUES ( '" + exec_id
				+ "','" + rule_id + "','" + execution_status + "','" + new Timestamp(System.currentTimeMillis())
				+ "')");
		hiveJdbcTemplate.execute(updateQuery.toString());

	}

	public void createAlertMasterTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema) {
		
		StringBuffer alertMaster = new StringBuffer();
		alertMaster.append("create table if not exists fincrime_" + hiveSchema + ".alert_master (")
		.append("exec_id String,alert_id String, alert_dt String,focal_entity_type String,"
				+ " focal_entity_id String, cust_id String, segment String, risk String )")
		.append(fieldSeparatorCreateStatement);

		hiveJdbcTemplate.execute(alertMaster.toString());
		log.debug("Alert Master Create table statement ran successfully");
	}

	public List<Rule> getApprovedRules(JdbcTemplate hiveJdbcTemplate, String runDate, String clientId) {

		StringBuffer sql = new StringBuffer();
		sql.append(
				"SELECT r.*,c.hiveSchema FROM rule r JOIN client c ON r.clientId=c.clientId where status='Approved' ");

		if (clientId != null) {
			sql.append("and r.clientId='" + clientId + "'");
		}

		List<Rule> approvedRules = hiveJdbcTemplate.query(sql.toString(),
				(rs, rowNum) -> new Rule(rs.getInt("ruleid"), rs.getString("rulename"),
						rs.getString("alertdetailcreatequery"), rs.getString("alertdetailinsertquery"),
						rs.getString("alerttransactionquery"), rs.getString("focalentity"), rs.getInt("clientid"),
						rs.getString("hiveSchema")));

		log.info("Rules list size=" + approvedRules.size());

		return approvedRules;

	}

	public void createRuleExecutionStatusTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema) {
		StringBuffer createTable = new StringBuffer();
		createTable.append("create table if not exists fincrime_" + hiveSchema + ".rule_execution_status ( ")
				.append("exec_id String, rule_id INT, execution_status String, execution_timestamp String )")
				.append(fieldSeparatorCreateStatement);

		hiveJdbcTemplate.execute(createTable.toString());

	}

	
public void createAlertTransactionTable(JdbcTemplate hiveJdbcTemplate, String hiveSchema) {
		
		StringBuffer alertTransactionCreateQuery = new StringBuffer();
		alertTransactionCreateQuery.append(" Create table if not exists fincrime_"+hiveSchema+".alert_transaction ( ")
				.append("exec_id String,tran_id String,tran_dt String,tran_amt FLOAT,tran_country String,")
				.append("opposite_account_number String,opposite_organization String,tcode_id String,")
				.append("acct_id String,acct_open_dt String,acct_segment String,acct_risk String,")
				.append("cust_id String,relation_eff_dt String,cust_segment String,cust_risk String,")
				.append("tcode_desc String,tran_type String,aml_monitoring_flag String,ctry_risk_score String )")
				.append(fieldSeparatorCreateStatement);
		
		log.info("alertTransactionCreateQuery=" + alertTransactionCreateQuery);
		hiveJdbcTemplate.execute(alertTransactionCreateQuery.toString());
		
	}

	public void insertAlertTransactionTable(JdbcTemplate hiveJdbcTemplate, String exec_id, int ruleId,
			String runDate, Rule rule) {
		String alerttransactionquery = rule.getAlerttransactionquery().replaceAll(Pattern.quote("${exec_id}"), exec_id)
				.replaceAll(Pattern.quote("${rule_id}"), String.valueOf(ruleId))
				.replaceAll(Pattern.quote("${runDate}"), runDate);
		
		log.info("alerttransactionquery=" + alerttransactionquery);
		hiveJdbcTemplate.execute(alerttransactionquery);
		
	}

	public void InsertIntoAlertDetailTable(JdbcTemplate hiveJdbcTemplate, String exec_id, int ruleId,
			String runDate, Rule rule, long currentTimestamp) {
		
		String alertdetailinsertquery = rule.getAlertdetailinsertquery()
				.replaceAll(Pattern.quote("${timestamp}"), String.valueOf(currentTimestamp))
				.replaceAll(Pattern.quote("${exec_id}"), exec_id)
				.replaceAll(Pattern.quote("${rule_id}"), String.valueOf(ruleId))
				.replaceAll(Pattern.quote("${runDate}"), runDate);
				//.replaceAll(Pattern.quote("null"), ruleName);
		
		log.info("alertdetailinsertquery=" + alertdetailinsertquery);
		hiveJdbcTemplate.execute(alertdetailinsertquery);
		
	}

	public void createAlertDetailTable(JdbcTemplate hiveJdbcTemplate, String exec_id, int ruleId,
			String runDate, Rule rule) {
		
		StringBuffer alertdetailcreatequery = new StringBuffer();
		alertdetailcreatequery.append(rule.getAlertdetailcreatequery()
				.replaceAll(Pattern.quote("${exec_id}"), exec_id)
				.replaceAll(Pattern.quote("${rule_id}"), String.valueOf(ruleId))
				.replaceAll(Pattern.quote("${runDate}"), runDate))
		.append(fieldSeparatorCreateStatement);
				//.replaceAll(Pattern.quote("null"), ruleName);
		
		log.info("alertdetailcreatequery=" + alertdetailcreatequery);
		hiveJdbcTemplate.execute(alertdetailcreatequery.toString());
		
	}

}
