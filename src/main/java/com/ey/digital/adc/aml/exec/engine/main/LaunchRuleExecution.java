package com.ey.digital.adc.aml.exec.engine.main;


import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.ey.digital.adc.aml.exec.engine.service.RuleExecutionService;

public class LaunchRuleExecution {

	private static final Logger log = Logger.getLogger(LaunchRuleExecution.class);

	public static void main(String[] args) {

		AbstractApplicationContext context = new ClassPathXmlApplicationContext(
				"/META-INF/spring/exec-engine-context.xml", LaunchRuleExecution.class);
		log.info("Rule Execution engine Running");
		context.registerShutdownHook();
		execute(context);
		context.close();
		log.info("Rule Execution Completed");

	}

	public static void execute(AbstractApplicationContext context) {
		RuleExecutionService ruleExecutionService = new RuleExecutionService();
		ruleExecutionService.executeEngine(context);
	}

}
