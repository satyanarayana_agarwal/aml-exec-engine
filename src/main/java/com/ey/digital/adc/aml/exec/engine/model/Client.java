package com.ey.digital.adc.aml.exec.engine.model;

import lombok.Data;

@Data
public class Client {

	private Integer clientId;
	private String name;
	private String hiveSchema;
	
	
	public Client(Integer clientId, String name, String hiveSchema) {
		super();
		this.clientId = clientId;
		this.name = name;
		this.hiveSchema = hiveSchema;
	}

}
