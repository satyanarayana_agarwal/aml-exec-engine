package com.ey.digital.adc.aml.exec.engine.model;

import lombok.Data;

@Data
public class Rule {

	private int ruleId;
	private String ruleName;
	private String alertdetailcreatequery;
	private String alertdetailinsertquery;
	private String alerttransactionquery;
	private String focalentity;
	private Integer clientId;
	private String hiveSchema;

	

	public Rule(int ruleId, String ruleName, String alertdetailcreatequery, String alertdetailinsertquery,
			String alerttransactionquery, String focalentity, Integer clientId, String hiveSchema) {

		this.ruleId = ruleId;
		this.ruleName = ruleName;
		this.alertdetailcreatequery = alertdetailcreatequery;
		this.alertdetailinsertquery = alertdetailinsertquery;
		this.alerttransactionquery = alerttransactionquery;
		this.focalentity = focalentity;
		this.clientId = clientId;
		this.hiveSchema = hiveSchema;
	}

	@Override
	public String toString() {
		return "Rule [ruleId=" + ruleId + ", ruleName=" + ruleName + ", alertdetailcreatequery="
				+ alertdetailcreatequery + ", alertdetailinsertquery=" + alertdetailinsertquery
				+ ", alerttransactionquery=" + alerttransactionquery + ", focalentity=" + focalentity + ", clientId="
				+ clientId + ", hiveSchema=" + hiveSchema + "]";
	}

	

}
