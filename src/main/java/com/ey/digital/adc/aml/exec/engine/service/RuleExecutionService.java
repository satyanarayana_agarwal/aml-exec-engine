package com.ey.digital.adc.aml.exec.engine.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import com.ey.digital.adc.aml.exec.engine.model.Client;
import com.ey.digital.adc.aml.exec.engine.model.Rule;
import com.ey.digital.adc.aml.exec.engine.repo.RuleEngineRepo;

public class RuleExecutionService {

	private static final Logger log = Logger.getLogger(RuleExecutionService.class);
	
	public void executeEngine(AbstractApplicationContext context) {

		RuleEngineRepo ruleEngineRepo = new RuleEngineRepo();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		// Subtract 365 days from current date since we only have transaction for the year 2016
		String runDate = dateFormat.format(new DateTime().minusDays(365).toDate());
		// runDate="12-30-2016";
		log.info("runDate= " + runDate);

		JdbcTemplate hiveJdbcTemplate = (JdbcTemplate) context.getBean("hiveJbdctemplate");

		String clientId = "10";

		// Rule Execution
		executeApprovedRulesinHive(hiveJdbcTemplate, ruleEngineRepo, runDate, clientId);

		// Case Rollup logic
		// Use caseTimestamp to build the caseId
		String caseTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
		// Subtract 365 days from current date since we only have transaction for the year 2016
		String caseDate = dateFormat.format(new DateTime().minusDays(365).toDate());

		rollupAlertsIntoCases(hiveJdbcTemplate, ruleEngineRepo, caseDate, caseTimestamp);
	}

	/**
	 * This method executes each approved rule for a client
	 * 
	 * @param hiveJdbcTemplate JdbcTemplate to connect to Hive
	 * @param ruleEngineRepo RuleEngineRepo contains queries that need to be executed for each rule
	 * @param runDate Run date for rule execution
	 * @param clientId Client Id
	 */
	private void executeApprovedRulesinHive(JdbcTemplate hiveJdbcTemplate, RuleEngineRepo ruleEngineRepo,
			String runDate, String clientId) {

		// Get a list of Approved Rules either for a specific client or all
		// clients

		List<Rule> approvedRules = ruleEngineRepo.getApprovedRules(hiveJdbcTemplate, runDate, clientId);

		// Execute Approved Rules for the given run date
		approvedRules.forEach(rule -> {

			String hiveSchema = rule.getHiveSchema();
			int ruleId = rule.getRuleId();
			// if (! (String.valueOf(ruleId).equalsIgnoreCase("1157") | String.valueOf(ruleId).equalsIgnoreCase("1158")))  return;
			// String ruleName = rule.getRuleName().replaceAll(Pattern.quote("
			// "), Pattern.quote("_"));
			String focalEntityType = rule.getFocalentity();
			String focalEntityId = "cust_id";
			String segment = "cust_segment";
			String risk = "cust_risk";
			// currentTimestamp will be used to build exec_id
			long currentTimestamp = new Timestamp(System.currentTimeMillis()).getTime();

			// Change certain field based on the focal entity
			if (rule.getFocalentity().equalsIgnoreCase("account")) {
				focalEntityId = "acct_id";
				segment = "acct_segment";
				risk = "acct_risk";
			}

			// Create Rule_Execution_status table if not exists
			ruleEngineRepo.createRuleExecutionStatusTable(hiveJdbcTemplate, hiveSchema);

			// Create Alert Master table in Hive if not exists
			ruleEngineRepo.createAlertMasterTable(hiveJdbcTemplate, hiveSchema);

			// create rule execution id for each rule
			String exec_id = ruleId + "_" + currentTimestamp;
			log.info("exec_id=" + exec_id);

			// Add InProgress execution entry for the rule in
			// Rule_execution_status table
			ruleEngineRepo.updateRuleExecutionStatus(hiveJdbcTemplate, hiveSchema, exec_id, ruleId,
					"InProgress");

			// create alert detail table if it does not exist
			ruleEngineRepo.createAlertDetailTable(hiveJdbcTemplate, exec_id, ruleId, runDate, rule);

			// Insert generated alerts into the Alert detail table
			ruleEngineRepo.InsertIntoAlertDetailTable(hiveJdbcTemplate, exec_id, ruleId, runDate, rule,
					currentTimestamp);

			ruleEngineRepo.createAlertTransactionTable(hiveJdbcTemplate, hiveSchema);

			// Insert all the underlying transactions for each alert
			ruleEngineRepo.insertAlertTransactionTable(hiveJdbcTemplate, exec_id, ruleId, runDate, rule);

			// Insert alerts into the Alert Master table
			ruleEngineRepo.insertAlertMasterTable(hiveJdbcTemplate, hiveSchema, exec_id, focalEntityType, focalEntityId,
					segment, risk, ruleId);

			// Add Complete execution entry for the rule in
			// Rule_execution_status table
			ruleEngineRepo.updateRuleExecutionStatus(hiveJdbcTemplate, hiveSchema, exec_id, ruleId,
					"Complete");

		});

	}

	/*
	 * For the given case date, for each customer, this method
	 * 1. rolls up all alerts into a case in the case_master table
	 * 2. Also, creates a case_alert table that assigns a case_id to each alert record
	 */
	/**
	 * For the given case date, for each customer, this method
	 * 1. rolls up all alerts for a customer into a case in the case_master table (rolled up table)
	 * 2. Also, creates a case_alert table that assigns a case_id to each alert for a customer(flat table)
	 * 
	 * @param hiveJdbcTemplate JdbcTemplate to connect to Hive
	 * @param ruleEngineRepo RuleEngineRepo contains queries that need to be executed for each rule
	 * @param caseDate Case date
	 * @param caseTimestamp Case timestamp
	 */
	private void rollupAlertsIntoCases(JdbcTemplate hiveJdbcTemplate, RuleEngineRepo ruleEngineRepo, String caseDate,
			String caseTimestamp) {

		// getAllClientSchemasFromHive(hiveJdbcTemplagte);
		// get a list of all clients from client table in Hive
		List<Client> clients = hiveJdbcTemplate.query("select clientId, name, hiveSchema from client",
				(rs, rowNum) -> new Client(rs.getInt("clientId"), rs.getString("name"), rs.getString("hiveSchema")));

		clients.forEach(client -> {

			String hiveSchema = client.getHiveSchema();

			ruleEngineRepo.createCaseMasterTable(hiveJdbcTemplate, hiveSchema);

			ruleEngineRepo.createCaseAlertTable(hiveJdbcTemplate, hiveSchema);

			ruleEngineRepo.insertCaseMasterTable(hiveJdbcTemplate, hiveSchema, caseDate, caseTimestamp);

			ruleEngineRepo.insertCaseAlertTable(hiveJdbcTemplate, hiveSchema, caseDate, caseTimestamp);

		});

	}

}
